# Installing COHERENS

COHERENS is a hydrodynamic model which is popular in European research community. It has a comprehensive manual. 

- Prerequisite: get `libhdf5_10_1.8.15.deb`, `libnetcdf7_4.4.0.deb`, `libnetcdff6_4.4.2.deb` (amd64 platform, if applicable). 

- Install the above files in order using e.g. `dpkg -i *.deb`  (install one by one, replacing * with the file name).

- Check COHERENS with `ldd coherens`.