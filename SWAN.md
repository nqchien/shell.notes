# Installing SWAN

## On Linux (Mint) desktop

- Be careful checking the platform/compiler specification in the Perl script.
  + Is that gfortran? If not, then set to `gfortran`.
- Make file by invoking `make serial`. You obtain `swan.exe`
- Copy this `swan.exe` to a subdirectory.
- Run the program by providing an input file as below
> ```./swanrun -input aninputfile```
- It is possible to run in parallel (MPI required):
> ```./swanrun -mpi 2 -input aninputfile```

## On Google Compute Engine (Ubuntu 12.04)

```
wget http://swanmodel.sourceforge.net/download/zip/swan4110.tar.gz
tar xzvf ...
cd ...
make config
make mpi
sudo make install
```