# Installing XBeach

## Basic installation
The installation process is straightforward. Issues the following commands into the terminal prompt:

- `svn https://svn.oss.deltares.nl/repos/xbeach/trunk`
- (move to the appropriate directory)
- `sudo apt-get install python-pip`
- `sudo pip install make`
- `sh autogen.sh`
- `./configure --with-mpi`
- `make`

## MPI parallel installation updated
This installation process is recommended by Arijen van Rooijen in (this thread)[https://oss.deltares.nl/web/xbeach/forum/-/message_boards/message/1003831].
which is basically:

```
$ sudo apt-get install subversion automake autoconf gfortran libnetcdf-dev libtool python-pip libopenmpi1.5-dev 

$ sudo pip install mako 

$ svn --username YOURNAMEonThisWebSite co https://svn.oss.deltares.nl/repos/xbeach/trunk

$ libtoolize --force
$ aclocal
$ autoheader
$ automake --force-missing --add-missing
$ autoconf
$ ./configure --with-mpi

$ make
$ sudo make install

$ svn co https://svn.oss.deltares.nl/repos/openearthtools
```

Beware though, the last checkout could take lots of space on your disk drive! You can cancel (Ctl-C) at any time.

### Post-installation
The problem is not over yet. After installation, invoking `xbeach` from the terminal would result in an error: 
```
xbeach: error while loading shared libraries: libxbeach.so.0: cannot open shared object file: No such file or directory
```
which I fixed as follows:
```
$ sudo ldconfig -v | grep xbeach
/sbin/ldconfig.real: Can't stat /usr/local/lib/x86_64-linux-gnu: No such file or directory
/sbin/ldconfig.real: Path `/lib/x86_64-linux-gnu' given more than once
/sbin/ldconfig.real: Path `/usr/lib/x86_64-linux-gnu' given more than once
/sbin/ldconfig.real: /lib/x86_64-linux-gnu/ld-2.27.so is the dynamic linker, ignoring

	libxbeach.so.0 -> libxbeach.so.0.0.0
```
to find out what links to `libxbeach.so.x` file.

```
$ sudo find / -name libxbeach.so.0.0.0
/usr/local/lib/libxbeach.so.0.0.0
^C
$LD_LIBRARY_PATH

$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
$ export LD_LIBRARY_PATH
```

Finally running XBeach with MPI (from the folder which contains XBeach input), for example:
```
$ mpirun -n 4 xbeach
$ mpirun -n 2 xbeach
```

Running with 4 processes will be twice faster on the current machine (core i9-9700), but it also produces noise.