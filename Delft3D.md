# Install Delft3D

* Browse to Deltares' Open Source section: `https://svn.oss.deltares.nl/`
* Enter your username and password (after you created a Deltares user account)
* Use a versioning system, e.g. svn:
> ```
> svn co https://svn.oss.deltares.nl/repos/delft3d/trunk/src/
> ```
* Install `hdf4`, `netcdf5` (bin, lib, devel)
* Download `netcdf-fortran` from the web and install 
* Run `./autogen.sh`
* Run `make`
* You might encounter errors while compiling the Fortran source. In such cases, try to replace any `SNGL(..)` function with `DBLE(..)`. Then re-make the source.
* _Don't_ `make install`.

## Update installation on Linux Mint 19

On 7-Jun-2021 I installed Delft3D on Linux Mint with the following adjustment:

- Install the libraries: 
  + `sudo apt install hdf4-tools`
  + `sudo apt install netcdf-bin`
  + `sudo apt install libnetcdff6 libnetcdff-dev`
- while running `./autogen.sh`, there is an error `libtoolize being run by autoreconf is not creating ltmain.sh in the auxillary directory like it should`.
- Just run `./configure` as recommended.
- Run `make`
- If there is an error relating to Swan (MPI?): ` *** No rule to make target 'swmod1.for', needed by 'swmod1.o'. Stop.` then you may move the `netcdf.mod` file from e.g. `/usr/include` to the desired directory shown bby the line preceding the error message.
- Now there is error at the source line `__open_too_many_args()` in file `fcntl2.h`.