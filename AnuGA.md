**AnuGA**, or AnuGA Hydro, is an open source solver to deal with 2-D shallow flows on earth surface. The program has been developed by Australia National University in collaboration with Geoscience Australia.

Index page: 
https://anuga.anu.edu.au 

The program can be installed on Windows, Linux, or run on Google's Colab web environment.

## On Windows

* Install AnuGA viewer (precompiled): 

  * obtain zip file, extract 
  * click `bin/viewer_run.bat`  —> default `../data/cairns.sww`
  * other `sww` file —> copy to this data directory and modify `viewer_run.bat` accordingly
  * Win7 32-bit on Intel: viewed ok
  * Win10 64-bit on AMD system: viewer showed heavily specked images, difficult to see 
        
* Install AnuGA: tried “Miniconda method”

  * Note that after running Miniconda installation, we must start “Anaconda prompt”
  * The prompt looks like this   `(base) C:\Users\name >` 
  * Follow `INSTALL.md` guide on GitHub to: 
        
     * install conda packages
     * (very important: not to drag the scrollbar the cmd window during installing packages)
     * Win10 64-bit on AMD system: takes long time to verify packages, but after that runs fine 
     * Win7 32-bit on Intel: not (yet) successful because local Internet connection was too slow
     * get git source -> anuga_core folder
     * build and install python code
     * run unittests 
     * run benchmark cases (very important; the generated files include demo for plotting results).
     * run examples; explore the results; output sww files can be copied to anuga_viewer/data folder 
     * this sww file is actually NetCDF (or zipped NetCDF? check) and can be viewed. On Linux, we can use command `ncdump`.
     * make your own `scenario.py` file 
     * run code (within Anaconda prompt) `python scenario.py` 
     * modify source code of anuga_core in subfolder anuga (Then is it necessary to rerun setup ?)
     * Carefully not to update code by using git pull or the modified code vanishes.
     * Maybe new environments must be created in conda to test different versions of modified code.


## Ubuntu 12.04 LTS (Google Compute Engine)

It is possible to install the `anuga` package

```
sudo pip install anuga
```

given the following dependencies

```
sudo apt-get install python-dev libpng libfreetype 
sudo pip install numpy scipy matplotlib
sudo easy_install -U distribute
sudo apt-get install gdal-bin libgdal1-dev libgdal1-...  libopenblas-base libopenblas-dev
```

If the error: "no lapack/blas resource found", accept it.

## Recommended way for Linux:

- Download and unzip the source file
- Invoke installation
  ```anuga_core $ bash tools/install_ubuntu.sh```
  (incremental comment code)
  
  This is successfully tried on HP elitebook Linux Mint 13 Maya 3.2.0-23 generic x86-64.
  
- Install Python NetCDF
  ```$ sudo apt-get install python-netcdf```
  
In summary, on Linux the writer had:

* Successfully tried anuga_core on a relatively recent version of Linux Mint (or equivalently Ubuntu)
* Successfully checked parallel run
* Install anuga_viewer from source is still uncertain (maybe error related to osg or netcdf, but this should be checked for the updated version of anuga_viewer for Linux).

## Colab environment 
This is a good testing environment for AnuGA

https://github.com/stoiver/anuga-clinic-2018 

