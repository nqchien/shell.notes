OpenFOAM is a popular open-source CFD solver. This note is based on https://openfoam.org/download/6-linux/ 

OpenFOAM6 is installed alongside paraviewopenfoam54

## The "no-docker" method 

I haven't tried it: https://openfoam.org/download/6-ubuntu/ 

It is claimed to be compatible with the following Ubuntu releases 

    - 14.04 LTS, codename trusty
    - 16.04 LTS, codename xenial = Mint 18.2
    - 18.04 LTS, codename bionic
    - 18.10, codename cosmic (from 30th November 2018)
    
## The Docker method 

### Install docker: 

```sudo apt install docker.io```

Then follow the notes for Debian:

```sudo usermod -aG docker $(whoami)```

### Installing OpenFOAM
```
sudo sh -c "wget http://dl.openfoam.org/docker/openfoam6-linux -O /usr/bin/openfoam6-linux"
sudo chmod 755 /usr/bin/openfoam6-linux
```

### Launching 
```
mkdir -p $HOME/OpenFOAM/${USER}-6
cd $HOME/OpenFOAM/${USER}-6
openfoam6-linux
```
The first time the container is launched, there is a short wait while the image downloads and unpacks.

### Testing openfoam6-linux

Follow the standard process to test the installation, first creating the run directory:
```
mkdir -p $FOAM_RUN
```
Copy across the backward facing step example, generate the mesh with blockMesh and run the steady flow, incompressible solver simpleFoam
```
cd $FOAM_RUN
cp -r $FOAM_TUTORIALS/incompressible/simpleFoam/pitzDaily .
cd pitzDaily
blockMesh
simpleFoam
paraFoam
```

Here on my platform I failed to display X screen and the following message appears:
```
qt.qpa.screen: QXcbConnection: Could not connect to display :0
Could not connect to any X display.
```
### Exiting Docker

When finished using OpenFOAM in Docker exit the environment by typing:

```exit```