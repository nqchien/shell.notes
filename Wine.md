**Wine** is a Windows emulator which runs on Linux and Mac OS X. Recently Wine support 64-bit programs. 

After installation, the `*.exe` files can be invoke using one of the following ways:

1. Double-click the icon in Wine File Manager. 

   To do this, first you have to type in the terminal: 
   
   `$ wine explorer`
   
2. Type in the terminal:

   `$ wine file.exe`

   The disadvantage of this method is that you cannot run `*.bat` files.
   
3. From the `cmd` Windows command: 
   
   `$ wine cmd`  
   
   Then use Windows (DOS) commands to navigate and execute programs.  
   
   This method combined with file editing in your native OS would be very effective.
   
NB. The Wine directory on your Mac should be somewhere at:
```
Library\Application Support/_12345..../drive_c/users/tld
```
