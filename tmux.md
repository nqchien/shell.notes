# Using tmux

`tmux` is an excellent window controller for UNIX environment. It should be used as a replacement for `screen`. 

Among the uses of `tmux`, I find controlling a UNIX-based server as a good example. 

- Install tmux
> `sudo apt-get update`
> `sudo apt-get install tmux`

- Run a script in background
  + `tmux new -s wavegrib.py`
  + Invoke `python wavegrib.py`
  + Detach window by `Ctl+b, d`

- Run another script in background
  + `tmux new -s wavecount.py`
  + Invoke `python wavecount.py`
  + Detach window by `Ctl+b, d`

- List windows
> `tmux ls`