# Install Gerris and GfsView on Linux Mint 18 (Sonya)

The Gerris Debian packages are available. It is straightforward to install using the following commands:
```
$ sudo apt-get install gerris gerris-mpi
$ sudo apt-get install libgfs-dev libgfs-1.3-2
$ sudo apt-get install gerris gfsview
```