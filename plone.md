## Starting Plone

On Ubuntu server, the old Plone system was on http://coastalstudy.uk (VPS hosted by InterServer, domain name from NameCheap). The performance has been very good, only few shutdowns have occurred during the past five years. However if such case occur, it is necesary to restart Plone by manually invoking through SSH:

```
cd /usr/local/Plone/zinstance/bin
sudo -u plone_daemon ./plonectl start
```

Updated: for the new IP address 51.68.220.203 (VPS hosted by OVH)

```
cd ~/Plone/zinstance/bin
sudo ./plonectl start
```

will show up the Plone CRM running on `http://51.68.220.203:8080/NCKHSV_KhoaB`.
