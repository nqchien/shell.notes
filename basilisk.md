Basilisk is an adaptive CFD solver. It is the successor of Gerris. 

I refer to Stephane Popinet's excellent official guide: http://www.basilisk.fr/src/INSTALL

## Obtaining Basilisk

### Via darcs
Installing darcs is as easy as
```
sudo apt-get install darcs flex make
```

To get the latest Basilisk source code do
```
darcs get --lazy http://basilisk.fr/basilisk
```

Alternatively you can install from an archive (compressed file).

### From archive 
```
wget http://basilisk.fr/basilisk/basilisk.tar.gz
tar xzf basilisk.tar.gz
```

## Compilation 

For compilation, the only requirement is a C99-compliant compiler and the make utility (a version of make compatible with GNU make is recommended but not essential).

If you are using gcc on a UNIX-like system, this should work:
```
cd basilisk/src
export BASILISK=$PWD
export PATH=$PATH:$PWD
ln -s config.gcc config
make -k
make
```
Note that on 32-bits systems you need to use `config.gcc.32bits` instead.

If you are using another system/compiler (for example Mac OSX), find a config file which looks close to yours, then try
```
cd basilisk/src
ls config.*
cp config.[your pick] config
make
```
You can also edit this file to customise things for your system (please also consider sharing your new config file).

To avoid having to re-type the two export commands above everytime you login, you should also add them to your $HOME/.bashrc file. You can either edit $HOME/.bashrc manually or do
```
cd basilisk/src
echo "export BASILISK=$PWD" >> ~/.bashrc
echo "export PATH=\$PATH:$BASILISK" >> ~/.bashrc
```

## Installing auxiliary tools
```
sudo apt-get install gnuplot imagemagick libav-tools smpeg-plaympeg graphviz valgrind gifsicle pstoedit
```
## Visualisation

- Online visualisation with Basilisk View requires a separate installation.
- Offline (interactive) visualisation requires the installation of the Basilisk View servers.

I chose off-screen rendering (no graphics card) as guided in http://www.basilisk.fr/src/gl/INSTALL

OSMesa is a software-only implementation of OpenGL i.e. it does not require any graphics hardware and is thus suitable for installation on large-scale clusters which usually do not have graphical capabilities. Both libGLU and libOSMesa are required.
Debian-based systems (Debian, Ubuntu, etc.)
```
sudo apt-get install bison libglu1-mesa-dev libosmesa6-dev
```
to install the required system libraries (and their “development” requirements i.e. header files etc.), then do:
```
cd $BASILISK/gl
make libglutils.a libfb_osmesa.a
```
to compile the libraries provided by Basilisk.

A short recipe for installation of both the interactive and non-interactive versions of Basilisk View is:
```
cd $BASILISK/gl
make
cd ..
make bview-servers
```

## Using Basilisk with python

You will need SWIG which can easily be installed with
```
sudo apt-get install swig libpython-dev
```
You also need to setup the `MDFLAGS` and `PYTHONINCLUDE` variables in your config file (not tried yet).


## Example 

Test the tutorial http://www.basilisk.fr/Tutorial 